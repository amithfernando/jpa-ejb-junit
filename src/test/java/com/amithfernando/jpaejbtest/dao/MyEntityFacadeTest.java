/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.jpaejbtest.dao;

import com.amithfernando.jpaejbtest.domains.MyEntity;
import javax.ejb.embeddable.EJBContainer;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Amith
 */
public class MyEntityFacadeTest {

    public MyEntityFacadeTest() {
    }

    @Test
    public void testVerify() throws Exception {
        System.out.println("verify");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        MyEntityFacade instance = (MyEntityFacade) container.getContext().lookup("java:global/classes/MyEntityFacade");
        System.out.println("Inserting entities...");
        instance.insert(5);
        int result = instance.verify();
        System.out.println("JPA call returned: " + result);
        System.out.println("Done calling EJB");
        Assert.assertTrue("Unexpected number of entities", (result == 5));
        container.close();
    }
    
    @Test
    public void insertTest() throws Exception {
        System.out.println("insert");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        MyEntityFacade instance = (MyEntityFacade) container.getContext().lookup("java:global/classes/MyEntityFacade");
        MyEntity myEntity=new MyEntity();
        myEntity.setName("Test");
        instance.create(myEntity);
        Assert.assertNotNull(myEntity.getId());
        container.close();
    }

}
