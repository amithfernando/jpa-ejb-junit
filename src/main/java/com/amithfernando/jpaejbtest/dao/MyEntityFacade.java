/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.jpaejbtest.dao;

import com.amithfernando.jpaejbtest.domains.MyEntity;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Amith
 */
@Stateless
public class MyEntityFacade extends AbstractFacade<MyEntity> {

    @PersistenceContext(unitName = "com.amithfernando_JpaEjbTest_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MyEntityFacade() {
        super(MyEntity.class);
    }


    public int verify() {
        String result = null;
        Query q = em.createNamedQuery("MyEntity.findAll");
        Collection entities = q.getResultList();
        int s = entities.size();
        for (Object o : entities) {
            MyEntity se = (MyEntity) o;
            System.out.println("Found: " + se.getName());
        }

        return s;
    }

    public void insert(int num) {
        for (int i = 1; i <= num; i++) {
            System.out.println("Inserting # " + i);
            MyEntity e = new MyEntity(i);
            em.persist(e);
        }
    }

}
